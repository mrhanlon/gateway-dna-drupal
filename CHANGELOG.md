# Changelog

## v0.2.2

2015-06-11

- Remove explicit SSLv3
- Ensure SSL_VERIFYPEER true

## v0.2.1

## v0.2.0

## v0.1.0
