<div data-service-id="<?php echo $service->id ?>">
  <h3><?php echo $service->name ?></h3>
  <ul class="list-unstyled">
  <?php foreach ($service->containers as $container) : ?>
    <li>
      <b><?php echo $container->name ?></b>:
      <span class="label label-<?php switch ($container->status_code) {
        case 100: echo "success"; break;
        case 200: echo "info"; break;
        case 300: echo "warning"; break;
        case 400: echo "warning"; break;
        case 500: echo "danger"; break;
        case 600: echo "danger"; break;
        default: echo "info"; break;
      } ?>"><?php echo $container->status ?></span>
    </li>
  <?php endforeach; ?>
  </ul>
</div>
