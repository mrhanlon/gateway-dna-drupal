# Agave Status

Basic module that provides monitoring of Agave API services using the
[Status IO API][1].

The module provides a page enumerating the status of each Agave service
and also provides a block which displays a roll-up, "least optimistic" value
for the entire Agave service.


[1]: http://kb.status.io/developers/status-codes
