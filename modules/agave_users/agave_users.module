<?php

function agave_users_menu(){
  $items = array();

  $items['admin/config/agave/users/manage'] = array(
    'title' => 'Manage Agave Users',
    'description' => 'Manage users in the Agave Tenant',
    'type' => MENU_NORMAL_ITEM,
    'page callback' => 'agave_users_admin_manage_api_users',
    'access arguments' => array('admin agave tenant'),
    'file' => 'agave_users.admin.inc'
  );

  $items['admin/config/agave/users/manage/authmap/%user'] = array(
    'title' => 'Manage Agave Users',
    'description' => 'Set user Authmaps',
    'type' => MENU_CALLBACK,
    'page callback' => 'agave_users_admin_set_agave_authmaps',
    'page arguments' => array(6),
    'access arguments' => array('admin agave tenant'),
    'file' => 'agave_users.admin.inc'
  );

  $items['user/%user/agave'] = array(
    'title' => t('Agave')
    , 'description' => t('Agave')
    , 'type' => MENU_LOCAL_TASK
    , 'page callback' => 'agave_users_user_token_management'
    , 'page arguments' => array(1)
    , 'access callback' => array('user_edit_access')
    , 'access arguments' => array(1)
  );

  $items['user/%user/agave/token'] = array(
    'title' => t('Token')
    , 'description' => t('Manage Your Agave Token')
    , 'type' => MENU_DEFAULT_LOCAL_TASK
    , 'access callback' => array('user_edit_access')
    , 'access arguments' => array(1)
    , 'weight' => -1000
  );

  $items['user/%user/agave/token/refresh'] = array(
    'title' => t('Agave Refresh Token')
    , 'description' => t('Refreshes the User\'s Agave Token')
    , 'type' => MENU_CALLBACK
    , 'page callback' => 'agave_users_user_token_refresh'
    , 'access callback' => array('user_edit_access')
    , 'access arguments' => array(1)
  );

  return $items;
}

function agave_users_user_token_management($account = NULL){
  global $user;
  if (!isset($account)) {
    $account = $user;
  }

  if (! empty($_SESSION['agave']['token'])) {
    // just connected!
    $tok = $_SESSION['agave']['token'];
    watchdog('agave_users', t('Successfully connected Agave for user %user via %flow'), array('%user' => $account->name, '%flow' => 'Authorization Flow'), WATCHDOG_INFO);
    agave_save_api_token($account, $tok);
    unset($_SESSION['agave']['token']);
  }

  $output = array();
  if ($token = agave_load_api_token($account)) {
    $output = _agave_users_user_token_management_display_token($account, $token);
  } else {
    $output = _agave_users_user_token_management_connect_agave($account);
  }

  return $output;
}

function _agave_users_user_token_management_display_token($account, $token){

  $tokenStateText = $token->isValid() ? t('Valid') : t('Expired');
  $tokenStateClass = $token->isValid() ? 'label-success' : 'label-danger';

  $output['intro'] = array(
    '#markup' => t('Current Agave Token')
    , '#prefix' => '<h2>'
    , '#suffix' => '</h2>'
  );

  $output['token'] = array(
    '#markup' =>
      t('Your current Agave token is: !tokenState @tokenExpiry',
        array(
          '!tokenState' => '<span class="label '.$tokenStateClass.'">'.$tokenStateText.'</span>'
          , '@tokenExpiry' => $token->isValid() ? t('until').' '.format_date($token->getCreated() + $token->getExpiresIn(), 'long') : ''
        )
      )
    , '#prefix' => '<p>'
    , '#suffix' => '</p>'
  );

  $output['token-value'] = array(
    '#markup' =>
      t('Token value: !token', array('!token' => '<code>'.$token->getAccessToken().'</code>'))
    , '#prefix' => '<p>'
    , '#suffix' => '</p>'
  );

  $actionLinks = array();
  $actionLinks[] = array(
    'title' => t('Refresh token now')
    , 'href' => 'user/'.$account->uid.'/agave/token/refresh'
    , 'attributes'=> array('class'=>'btn btn-default')
  );
  $output['actions'] = array(
    '#links' => $actionLinks
  , '#heading' => array('text' => t('Actions'), 'level' => 'h4')
  , '#attributes' => array('class' => array('links', 'inline'))
  , '#theme' => 'links'
  , '#prefix' => '<hr>'
  );
  return $output;
}

function _agave_users_user_token_management_connect_agave($account = NULL){
  $output['header'] = array(
    '#markup' => t('Connect your Agave Account')
    , '#prefix' => '<h2>'
    , '#suffix' => '</h2>'
  );
  $output['intro'] = array(
    '#markup' => t('In order to access any Agave-managed resources you will need to authorize the Arabidopsis Information Portal to use the Agave API on your behalf.')
    , '#prefix' => '<p>'
    , '#suffix' => '</p>'
  );
  $output['form'] = drupal_get_form('agave_users_token_form');

  return $output;
}

function agave_users_token_form($form, &$form_state) {
  global $user;

  return array(
    'uid' => array(
      '#type' => 'value',
      '#value' => $user->uid),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Connect Agave Account'),
      '#attributes' => array('class' => array('btn-success'))
      )
  );
}

function agave_users_token_form_submit($form, &$form_state) {
  $uid = $form_state['values']['uid'];
  $_SESSION['agave']['redirect'] = "user/$uid/agave";
  $_SESSION['agave']['error_redirect'] = "user/$uid/agave";
  drupal_goto('agave/login');
}

function agave_users_user_token_refresh(){
  global $user;
  $token = agave_load_api_token($user);
  $token->refresh();
  agave_save_api_token($user, $token);
  drupal_set_message(t('Your Agave Token has been successfully refreshed!'));
  drupal_goto('user/'.$user->uid.'/agave/token');
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function agave_users_form_user_login_block_alter(&$form, &$form_state) {
  _agave_users_user_login_form_alter($form, $form_state);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function agave_users_form_user_login_alter(&$form, &$form_state) {
  _agave_users_user_login_form_alter($form, $form_state);
}

/**
 * Replace the default login validation with Agave API call
 * TODO: need admin config to enable/disable?
 * TODO: need admin switch to toggle mixed login
 */
function _agave_users_user_login_form_alter(&$form, &$form_state) {
  if (agave_tenant_configured()) {

    // TODO this should be extracted to a variable that is controllable by an admin
    // replace user_login_authenticate_validate with our validate
    $form['#validate'][1] = 'agave_users_user_login_validate';
  }
}

/**
 * Do Agave login validation, try to get a token
 */
function agave_users_user_login_validate($form, &$form_state) {

  $name = $form_state['values']['name'];
  $pass = $form_state['values']['pass'];

  if ($uid = _agave_users_token_login($name, $pass)) {
    $form_state['uid'] = $uid;
  }
}

/**
 * After the user has logged in, check that user in Agave Tenant is configured.
 *
 * TODO: should we have an admin switch for this? should we punt this to the users
 * instead of doing it auto? both?
 */
function agave_users_user_login(&$edit, $account) {
  if (agave_tenant_configured()) {
    $authmaps = user_get_authmaps($account->name);
    if (!$authmaps || !array_key_exists('agave_users', $authmaps)) {
      // does user exist in Agave Tenant?
      if ($agaveUser = AgaveUser::findUser($account->name)) {
        watchdog('agave_users'
          , t('Remote user account already exists in Agave tenant %tenant for %user; setting auth maps', array('%tenant' => variable_get('agave_tenant_base_url', '[error tenant missing!]'),'%user' => $account->name))
          , NULL
          , WATCHDOG_INFO);
        user_set_authmaps($account, array('authname_agave_users' => $account->name));
      } else {
        watchdog('agave_users'
          , t('Creating user account in Agave tenant %tenant for %user', array('%tenant' => variable_get('agave_tenant_base_url', '[error tenant missing!]'),'%user' => $account->name))
          , NULL
          , WATCHDOG_INFO);

        try {
          // create user in API tenant
          $user = new AgaveUser($account->name, $account->mail);
          $pass = NULL;
          if ($edit && array_key_exists('values', $edit) && array_key_exists('pass', $edit['values'])) {
            $pass = $edit['values']['pass'];
          } else {
            // user must have logged in with a OTP link
            $pass = user_password();
          }
          $user->save($pass);

          // set authmaps
          user_set_authmaps($account, array('authname_agave_users' => $account->name));
          $account = user_external_load($account->name);

          // only get a token if user has logged in before, otherwise we'll get on once password is set
          if ($account->access != 0) {
            $token = AgaveToken::newToken('password', array('name' => $account->name, 'pass' => $pass));
            agave_save_api_token($account, $token);
          }
        } catch (Exception $e) {
          watchdog('agave_users'
            , t('An error occurred while creating user account in Agave tenant %tenant for %user.', array('%tenant' => variable_get('agave_tenant_base_url', '[error tenant missing!]'),'%user' => $account->name)) . $e->getMessage()
            , NULL
            , WATCHDOG_ERROR);
        }
      }
    }
  }
}

function _agave_users_token_login($name, $pass) {
  try {
    if ($token = AgaveToken::newToken('password', array('name' => $name, 'pass' => $pass))) {
      // user validated against the API; load local Drupal account
      $user = user_external_load($name);
      if (! $user) {
        // might have been created by administrator
        $user = user_load_by_name($name);
        if ($user) {
          user_set_authmaps($user, array('authname_agave_users' => $name));
          $user = user_external_load($name);
        } else {
          // account created outside of Drupal; create local account
          user_external_login_register($name, 'agave_users');
          $user = user_external_load($name);
        }
      }

      // persist token to DB for later use
      agave_save_api_token($user, $token);

      // make sure local Drupal user has latest password
      user_save($user, array('pass' => $pass));

      // return uid
      return $user->uid;
    } else {
      watchdog('agave_users'
        , t('Unable to obtain token for %user. Bad username/password.', array('%user' => $name))
        , NULL
        , WATCHDOG_NOTICE);
    }
  } catch (Exception $e) {
    watchdog('agave_users'
      , t('Unable to obtain token for %user. Agave login failed: @error', array('%user' => $name, '@error' => $e->getMessage()))
      , NULL
      , WATCHDOG_NOTICE);
  }
  return FALSE;
}

/***
 * hook_form_user_register_alter
 * Add our validation function to the form processing
 */
function agave_users_form_user_register_form_alter(&$form, &$form_state){

  // alter username requirements for Agave LDAP
  $form['account']['name']['#description'] = t('Username must have at least 3 characters. Lowercase only. First character must be a lowercase letter. May only contain lowercase letters, numbers, dashes (-) and underscores (_).');

  array_unshift($form['#validate'], 'agave_users_user_register_validate');
}

function agave_users_user_register_validate($form, &$form_state){
  $name = $form_state['values']['name'];
  $email = $form_state['values']['mail'];

  if (! preg_match('/^[a-z][a-z0-9\-_]{2,59}$/', $name)) {
    form_set_error('name', t('The name %name is invalid. Please review the username requirements.', array('%name' => $name)));
  } else if ($user = AgaveUser::findUser($name)) {
    form_set_error('name', t('The name %name is already taken.', array('%name' => $name)));
  }
}

// TODO handle registration success and initial token grab when variable_get('user_email_verification', TRUE) === FALSE

function agave_users_form_user_profile_form_alter(&$form, &$form_state){
  // hook when users update their account
  if ($form['#user_category'] === 'account') {
    // alter username requirements
    $form['account']['name']['#description'] = t('Username must have at least 3 characters. Lowercase only. First character must be a lowercase letter. May only contain lowercase letters, numbers, dashes (-) and underscores (_).');
    array_unshift($form['#validate'], 'agave_users_form_user_profile_form_validate');
    array_unshift($form['#submit'], 'agave_users_form_user_profile_form_submit');
  }
}

function agave_users_form_user_profile_form_validate($form, &$form_state) {
  $name = $form_state['values']['name'];
  $pass = $form_state['values']['pass'];
  $mail = $form_state['values']['mail'];

  if (! preg_match('/^[a-z][a-z0-9\-_]{2,59}$/', $name)) {
    form_set_error('name', t('The name %name is invalid. Please review the username requirements.', array('%name' => $name)));
  }

  $agaveUser = AgaveUser::findUser($name);
  if ($agaveUser) {
    $agaveUser->setEmail($mail);

    try {
      $agaveUser->save($pass);
    } catch (Exception $e) {
      watchdog('agave_users',
        t('Error saving user %user: @message'),
        array('%user' => $name, '@message' => $e->getMessage()),
        WATCHDOG_ERROR);

      form_set_error('pass', $e->getMessage());
    }
  }
}

function agave_users_form_user_profile_form_submit($form, &$form_state){
  $name = $form_state['values']['name'];
  $pass = $form_state['values']['pass'];
  if ($pass) {
    // user changed password, may be first setting, check for token
    $user = user_load_by_name($name);
    $token = agave_load_api_token($user);
    if (! $token) {
      $token = AgaveToken::newToken('password', array('name' => $name, 'pass' => $pass));
      agave_save_api_token($user, $token);
    }
  }
}
