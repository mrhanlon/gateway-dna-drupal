# Agave Gateway DNA: Users

This module includes support for the Agave Users API. It provides
support for an Agave-backed Drupal gateway, mixed local and remote
authentication (coming soon), and persisting user profiles to the
backing Agave User store.