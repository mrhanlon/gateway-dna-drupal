<?php
/**
 * AgaveFile.php
 * @author mock
 */
class AgaveFile {

  /**
   * @param string $apiKey your API key
   * @param string $apiServer the address of the API server
   */
  function __construct($agaveToken, $apiServer) {
    $this->agaveToken = $agaveToken;
    $this->apiServer = $apiServer;
error_log(print_r($agaveToken, 1));
error_log(print_r($apiServer, 1));
  }

  /**
   * @param string $resourcePath path to method endpoint
   * @param string $method method to call
   * @return unknown
   */
  public function getFile($storageSystem, $filePath) {
      global $user;
      $token = $this->agaveToken;
      $base_url = $this->_getBaseFileDownloadURL();
      if ($token && $token->isValid()) {
        try {
          $apiToken = variable_get('agave_tenant_token', ''); //master token
          $accessToken = $token->getAccessToken(); //shouldn't have to do this
error_log('accessToken =' . $accessToken);
error_log('path = ' .$base_url.$storageSystem.$filePath);
error_log('apiToken=' . $apiToken);
          $client = new APIClient($accessToken, $this->apiServer);
          // should be this one, using $apiToken because something broken with Agavae: $apiResp = $client->callAPI($base_url.$storageSystem.$filePath, 'GET', array(), null, array('Authorization' => "Bearer ". $accessToken));
          $apiResp = $client->callAPI($base_url.$storageSystem.$filePath, 'GET', array(), null, array('Authorization' => "Bearer ". $apiToken));
          return $apiResp;
        } catch (Exception $e) {
            error_log('problem in AgaveFile getFile()');
            watchdog('agave_files'
              , t('An error occurred while fetching file %file for %user from system %system.', array('%file' => $filePath,'%user' => $user->name, '%system' =>$storageSystem)) . $e->getMessage()
              , NULL
              , WATCHDOG_ERROR);
        } //end try/catch
      }//end if
      return FALSE;
  }

  function _getBaseFileDownloadURL() {
    return '/files/v2/media/system/';
  }

  function _getBaseFileListURL() {
    return '/files/v2/listings/system/';
  }

}
