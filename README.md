# Agave Gateway DNA: Core

This module is the base component for using Agave Gateway DNA in your
Drupal site.

## Installation

Install the module as you would any Drupal module. Clone the module
into the `sites/all/modules` directory in your Drupal installation.


## Configuration

Once the module is installed you will need to configure the API Tenant. Go to `http://example.com/admin/config/agave/tenant`. There are five fields that need to be configured:

Name                | Variable name                | Description
--------------------|------------------------------|------------
API Tenant Base URL | agave_tenant_base_url        | The Base URL for your API tenant. For example, https://agave.iplantc.org.
Consumer Key        | agave_tenant_consumer_key    | Your Consumer Key. Obtained from the Tenant API Store.
Consumer Secret     | agave_tenant_consumer_secret | Your Consumer Secret. Obtained from the Tenant API Store.
Username            | agave_tenant_username        | Privileged user username (for creating users in the Tenant)
Token or Password   | agave_tenant_token           | Privileged user token or password (for creating users in the Tenant)

You will need to visit your Tenant's API Store to create an application and subscribe to the APIs. The Store should be accessible at $API_BASE_URL/store, for example, https://agave.iplantc.org/store.
